import { InputStream, CommonTokenStream } from 'antlr4';
import * as fs from 'fs-extra';
// import { DicomMessage, DicomMetaDictionary, datasetToBlob } from 'dcmjs';
import { DE6Parser } from './parser/DE6Parser';
import { DE6Lexer } from './parser/DE6Lexer';

const DicomMessage = require('dcmjs').data;
const DicomMetaDictionary = require('dcmjs').data;
const datasetToBlob = require('dcmjs').data;

const parser = new DE6Parser(new CommonTokenStream(new DE6Lexer(new InputStream('version "6.1"\n(0010,0010) := "foo"'))));
const script = parser.script();

// eslint-disable-next-line no-console
console.log(script);

const dicomData = DicomMessage.readFile(fs.readFileSync('/Users/rherrick/Pictures/DICOM/sample2/1/1.2.840.113654.2.45.2.108105-1-1-2z65o7.dcm'));
const dataset = DicomMetaDictionary.naturalizeDataset(dicomData.dict);
// eslint-disable-next-line no-underscore-dangle
dataset._meta = DicomMetaDictionary.namifyDataset(dicomData.meta);

const dcmBlob = datasetToBlob(dataset);
fs.writeFileSync('/Users/rherrick/Pictures/DICOM/sample2-anon/1/1.2.840.113654.2.45.2.108105-1-1-2z65o7.dcm', dcmBlob);
