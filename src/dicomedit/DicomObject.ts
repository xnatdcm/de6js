import * as fs from 'fs';
import * as dicomParser from 'dicom-parser';
import { DataSet } from 'dicom-parser';
import { DicomObjectI } from './DicomObjectI';

export class DicomObject implements DicomObjectI {
    dataSet: DataSet;

    constructor() {
        this.dataSet = Object.create(null);
    }

    getAttribute(tag: string) {
        return `tag ${tag}: ${this.dataSet.string(tag)}`;
    }

    // Read the DICOM P10 file from disk into a Buffer
    // var filePath = process.argv[2] || 'ctimage.dcm';
    readFile(filePath: string) {
        console.log('File Path = ', filePath);
        const dicomFileAsBuffer = fs.readFileSync(filePath);

        try {
            // Parse the dicom file
            this.dataSet = dicomParser.parseDicom(dicomFileAsBuffer);
            // print the patient's name
            const patientName = this.dataSet.string('x00100010');
            console.log(`Patient Name = ${patientName}`);
            // Get the pixel data element and calculate the SHA1 hash for its data
            const pixelData = this.dataSet.elements.x7fe00010;
            // eslint-disable-next-line max-len
            const pixelDataBuffer = dicomParser.sharedCopy(dicomFileAsBuffer, pixelData.dataOffset, pixelData.length);
            console.log('Pixel Data length = ', pixelDataBuffer.length);
        } catch (ex) {
            console.log(ex);
        }
    }
}
