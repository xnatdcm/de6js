export interface DicomObjectI {

     getAttribute( tag: string): string
     readFile( filePath: string): void

}
