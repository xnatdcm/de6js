import { ScriptApplicatorI } from './ScriptApplicatorI';
import { DicomObjectI } from './DicomObjectI';
import { DicomObject } from './DicomObject'

export class ScriptApplicator implements ScriptApplicatorI {
    script: string

    constructor(script: string) {
        this.script = script;
    }

    apply(file: string): DicomObjectI {
        const dobj = new DicomObject();
        dobj.readFile(file);
        console.log(`Apply script ${this.script} to file ${file}`);
        console.log(dobj.getAttribute('x00100010'));
        return dobj;
    }
}
