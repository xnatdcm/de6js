import { InputStream, CommonTokenStream } from 'antlr4';
import { DE6Parser } from './parser/DE6Parser';
import { DE6Lexer } from './parser/DE6Lexer';
import { ScriptApplicator } from './dicomedit/ScriptApplicator';

const parser = new DE6Parser(new CommonTokenStream(new DE6Lexer(new InputStream('version "6.1"\n(0010,0010) := "foo"'))));
const script = parser.script();
console.log(script);

const filePath = '/Users/drm/Downloads/aaa_CT_1/123456/DICOM/1.2.826.0.1.3680043.2.1125.1.31055836655812439933974091049514162-123456-1-13h21uu.dcm';

const applicator = new ScriptApplicator('script');
const dobj = applicator.apply(filePath);
console.log(`Patient ID: ${dobj.getAttribute('x00100020')}`);
