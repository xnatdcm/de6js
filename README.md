DICOM Edit 6 JavaScript
================================

DICOM Edit 6 JavaScript (de6js) provides support for extracting and modifying DICOM header metadata and values in DICOM
data. This JavaScript implementation of DICOM Edit 6 uses the same grammar as the original [Java library][1], parses it
using [antlr4-tool][2] to generate Typescript code for the parser, and builds a library very similar to the existing 
Java-based functionality for DICOM Edit 6.

Building
--------

To build **de6js**:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$ yarn install
$ yarn build-parser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**antlr4-tool** will create a number of Typescript and JavaScript files in the [parser](src/parser) folder:

* DE6Lexer.d.ts
* DE6Lexer.js
* DE6Parser.d.ts
* DE6Parser.js
* DE6ParserListener.d.ts
* DE6ParserListener.js
* DE6ParserVisitor.js
* TagPathLexer.d.ts
* TagPathLexer.js
* TagPathListener.d.ts
* TagPathListener.js
* TagPathParser.d.ts
* TagPathParser.js
* TagPathVisitor.js

The main script in [src/index.ts] can reference these:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import {InputStream, CommonTokenStream} from 'antlr4';
import {DE6Parser} from './parser/DE6Parser';
import {DE6Lexer} from './parser/DE6Lexer';

const parser = new DE6Parser(new CommonTokenStream(new DE6Lexer(new InputStream(''))))
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
Consult the [documentation for the DICOM Edit 6 script syntax][3] for more information on
writing anonymization scripts. 

[1]: https://bitbucket.org/xnatdcm/dicom-edit6
[2]: https://github.com/mcchatman8009/antlr4-tool
[3]: https://wiki.xnat.org/display/XTOOLS/DicomEdit+6.2+Language+Reference
